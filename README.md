# @netfront/kanzi-ionic-tool

> Reusable components

## Install

```bash
npm install --save @netfront/kanzi-ionic-tool
```

```bash
yarn add @netfront/kanzi-ionic-tool
```

## Usage

Into the app.component.ts file, import and initialise the tool as followed.

```ts

import * as kanzi from "@netfront/kanzi-tool-ionic";

constructor(){
 kanzi.init(
      "API_KEY",
      "DEFAULT_LANGUAGE",
      "PACKAGE_NAME",
    );
}

```

## License

MIT © [Netfront](https://netfront.com.au)
